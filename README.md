# Docker setup for PHP projects #

This setup includes PHP-FMP 8.1, Mysql 8, PhpMyAdmin and Nginx

### Setup ###

1. fork repository and rename
2. Clone project into your project directory on your computer
3. Enter `cd docker` in terminal
4. Enter `docker compose up` in terminal
5. Go to http://localhost
6. Open phpMyAdmin http://localhost:81
7. Login to phpMyAdmin
   * server: mysql
   * user: root
   * pass: secret
8. Setup your php project into /public folder
